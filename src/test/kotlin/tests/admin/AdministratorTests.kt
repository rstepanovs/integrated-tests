package tests.admin

import io.restassured.http.Method
import lv.romans.meters.commons.Address
import lv.romans.meters.commons.Constants.PASSWORD_CHANGE_REQUIRED
import lv.romans.meters.commons.api.AdminDashboard
import lv.romans.meters.commons.api.ChangePassword
import lv.romans.meters.commons.api.NewManager
import lv.romans.meters.commons.api.NewProperty
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import tests.ApiSpec
import tests.ApiSpec.baseSpec
import tests.Extensions.addBearer
import tests.Extensions.response
import tests.Extensions.token
import tests.config.Admin
import tests.config.FirstTimeAdmin
import tests.config.Manager

class AdministratorTests {


    @Test
    fun `after first login administrator is requested to change password`() {
        assertTrue(FirstTimeAdmin().login().andReturn().token().getClaim(PASSWORD_CHANGE_REQUIRED).asBoolean())
    }

    @Test
    fun `after first login administrator changes password`() {
        val initialToken = FirstTimeAdmin().login().andReturn().token()
        //First time admin changes to admin
        val newToken = baseSpec.body(ChangePassword("admin123", "admin123"))
            .addBearer(initialToken).request(Method.POST, "changePassword").token()
        assertTrue(newToken.getClaim(PASSWORD_CHANGE_REQUIRED).asBoolean().not())

        //cleanup
        baseSpec.body(FirstTimeAdmin().password).addBearer(newToken)
            .request(Method.POST, "test/revertPasswordChange")
            .andReturn()
    }

    @Test
    fun `administrator receives empty dashboard`() {
        val initialToken = Admin().login().andReturn().token()

        val newUserDashboard = ApiSpec.baseSpec.addBearer(initialToken)
            .request(Method.GET, "admin/dashboard").andReturn().response<AdminDashboard>()

        assertNotNull(newUserDashboard)
    }

    @Test
    fun `administrator can create manager and receive it in a list of managers`() {

        with(Admin()) {
            val newManager = NewManager("Random Guy", "raandomGuy@resttest.com", "+37121133333")

            createManager(newManager).then().statusCode(201)

            val newManagerInfo = getManagers().managers
            assertTrue(newManagerInfo.find { it.email == newManager.email } != null)

            //Cleanup
            deleteManager(newManager.email)
        }
    }

    @Test
    fun `administrator can create new property and receive it in the list of all properties`() {

        with(Admin()) {
            val property = NewProperty(Address("Riga", "Random street", 12), Manager().email)
            val propertyId = createProperty(property)
            val dashboard = dashboard()

            val createdProperty = dashboard.properties.find { it.id == propertyId }!!

            assertThat(property.address, equalTo(createdProperty.address))
            assertThat(property.managerEmail, equalTo(property.managerEmail))
        }

    }
    @Test
    fun `administrator can change manager for property`() {

        with(Admin()) {
            val property = dashboard().properties[0]
            val allManagers = getManagers().managers
            val otherManager = allManagers.first { it.email != property.managerEmail}

            changeManager(property.id, otherManager.email)

            assertThat(dashboard().properties.find { it.id == property.id }!!.managerEmail, equalTo(otherManager.email))

            //Cleanup
            changeManager(property.id, property.managerEmail)
        }
    }
}