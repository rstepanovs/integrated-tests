package tests

import com.auth0.jwt.JWT
import com.auth0.jwt.interfaces.DecodedJWT
import io.restassured.RestAssured.given
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType
import io.restassured.mapper.ObjectMapperType
import io.restassured.response.Response
import io.restassured.specification.RequestSpecification
import lv.romans.meters.commons.Constants

object ApiSpec {
    val spec = RequestSpecBuilder()
        .setBaseUri("http://localhost:8741")
        .setBasePath("api")
        .setContentType(ContentType.JSON)
        .build()

    val baseSpec: RequestSpecification
        get() = given().spec(spec)
}

object Extensions {

    fun Response.token() = header(Constants.HEADER_AUTHORIZATION).replace(Constants.AUTH_PREFIX, "").run { JWT.decode(this) }

    inline fun <reified T> Response.response(): T {
        val response = this.body.`as`<Any>(T::class.java, ObjectMapperType.GSON)
        return response as T
    }

    fun RequestSpecification.addBearer(jwt: DecodedJWT) = header(Constants.HEADER_AUTHORIZATION, "${Constants.AUTH_PREFIX}${jwt.token}")
}