package tests.manager

import io.restassured.http.Method
import lv.romans.meters.commons.Constants
import lv.romans.meters.commons.api.*
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.empty
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import tests.ApiSpec.baseSpec
import tests.Extensions.addBearer
import tests.Extensions.token
import tests.config.FirstTimeManager
import tests.config.Manager
import tests.config.Standard
import java.time.Instant
import java.time.Period
import java.util.*

class ManagerTests {
    @Test
    fun `after first login manager is requested to change password`() {
        Assertions.assertTrue(
            FirstTimeManager().login().andReturn().token().getClaim(Constants.PASSWORD_CHANGE_REQUIRED).asBoolean()
        )
    }

    @Test
    fun `after first login manager changes password`() {
        val initialToken = FirstTimeManager().login().andReturn().token()
        //First time admin changes to admin
        val newToken = baseSpec.body(ChangePassword("password2", "password2"))
            .addBearer(initialToken).request(Method.POST, "changePassword").token()
        Assertions.assertTrue(newToken.getClaim(Constants.PASSWORD_CHANGE_REQUIRED).asBoolean().not())

        //cleanup
        baseSpec.body("FirstTimeManager().password").addBearer(newToken)
            .request(Method.POST, "test/revertPasswordChange").then().statusCode(204)
    }

    @Test
    fun `manager can receive list of his managed properties`() {
        val dashboard = Manager().dashboard()
        val propertyAddresses = dashboard.properties.map { it.address }
        assertThat(propertyAddresses.size, equalTo(3))
        assertThat(propertyAddresses.intersect(Manager.managerDefaultProperties).size, equalTo(3))
    }

    @Test
    fun `manager can set up new property`() {
        with(Manager()) {
            val notSetupProperty = dashboard().properties[0].id
            val form = PropertyForm(21 to 31, 1 to 12)
            finishSetup(notSetupProperty, form)

            val newState = getSetupState(notSetupProperty)
            assertThat(newState.billDates, equalTo(form.billDates))
            assertThat(newState.submitDates, equalTo(form.submitDates))
            assertThat(newState.apartments, empty())

            //Cleanup
            baseSpec.addBearer(token)
                .pathParams("propertyId", notSetupProperty)
                .request(Method.GET, "test/resetProperty/{propertyId}")
                .andReturn()
        }
    }

    @Test
    fun `manager can create new apartment`() {
        with(Manager()) {
            val readyProperty = dashboard().properties[2].id

            val originalState = getSetupState(readyProperty)
            val newApartmentNumber = originalState.apartments.map { it.number }.max() ?: 1
            val owner = OwnerInfo("Main User", Standard().email, null)
            val meters = listOf(ApartmentSetupMeter("Cold water", UUID.randomUUID().toString(), 14.0F,
                Date.from(Instant.now().plus(Period.ofDays(365)))))
            val newApartmentId = createApartment(readyProperty, ApartmentForm(0, newApartmentNumber, owner, meters))

            val newState = getSetupState(readyProperty)

            val savedApartment = newState.apartments.find { it.id == newApartmentId }
            assertNotNull(savedApartment)

        }
    }

}