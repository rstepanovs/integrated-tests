package tests.config

import io.restassured.http.Method
import io.restassured.response.Response
import io.restassured.specification.RequestSpecification
import lv.romans.meters.commons.Address
import lv.romans.meters.commons.api.*
import tests.ApiSpec.baseSpec
import tests.Extensions.addBearer
import tests.Extensions.response
import tests.Extensions.token

open class User(val email: String, val password: String, val role: String) {
    val token by lazy { login().andReturn().token() }

    fun login() = baseSpec.body(Login(email, password)).request(Method.POST, "login")

    val authorized: RequestSpecification
        get() = baseSpec.addBearer(token)
}

class Standard : User("authtestuser@resttest.com", "password1", "ROLE_USER")



class Manager : User("authtestmanager@resttest.com", "password2", "ROLE_MANAGER") {

    companion object {
        val managerDefaultProperties = listOf(
            Address("Riga", "Raina bulvaris", 19), //Not set up
            Address("Riga", "Raina bulvaris", 21), // Not set up
            Address("Riga", "Raina bulvaris", 23) //Set up
        )
    }

    fun dashboard(): ManagerDashboard =
        baseSpec.addBearer(token).request(Method.GET, "properties/my").andReturn().response()

    fun getSetupState(propertyId: Long): PropertySetup =
        authorized.queryParam("id", propertyId)
            .request(Method.GET, "properties/setup").andReturn().response()

    fun finishSetup(propertyId: Long, form: PropertyForm) {
        authorized.pathParams("propertyId", propertyId).body(form)
            .request(Method.PATCH, "properties/complete/{propertyId}")
            .andReturn()
    }

    fun createApartment(propertyId: Long, form: ApartmentForm) =
        authorized.queryParam("propertyId", propertyId).body(form)
            .request(Method.POST, "apartments")
            .andReturn().response<Long>()
}

class Admin : User("admin@testmanagers.lv", "admin123", "ROLE_ADMIN") {
    fun dashboard(): AdminDashboard =
        authorized.request(Method.GET, "admin/dashboard").andReturn().response()

    fun getManagers(): ManagerList =
        authorized.request(Method.GET, "managers/list").andReturn().response()

    fun createManager(manager: NewManager) =
        authorized.body(manager).request(Method.POST, "managers/add")

    fun deleteManager(managerEmail: String) =
        authorized.queryParam("manager", managerEmail)
            .request(Method.DELETE, "managers").andReturn()

    fun createProperty(property: NewProperty) =
        authorized.body(property).request(Method.POST, "properties/add").andReturn().response<Long>()

    fun changeManager(propertyId: Long, newManagerEmail: String): Response {
        return authorized.queryParam("id", propertyId)
            .queryParam("manager", newManagerEmail)
            .request(Method.PATCH, "properties").andReturn()
    }
}

class FirstTimeAdmin : User("firstTimeAdmin@testmanagers.lv", "newAdminPassword", "ROLE_ADMIN")
class FirstTimeManager : User("firstTimeManager@testmanagers.lv", "ec481b86cd", "ROLE_ADMIN")
